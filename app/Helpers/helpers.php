<?php
// Clean string, remove new lines
function cleanString($string){
    return trim(preg_replace('/(\v|\s)+/', ' ', $string));
}
