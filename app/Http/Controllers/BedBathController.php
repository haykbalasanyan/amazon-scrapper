<?php

namespace App\Http\Controllers;

use voku\helper\HtmlDomParser;

class BedBathController extends Controller
{
    private $html;

    public function __construct($html)
    {
        $this->html = $html;
    }

    public function scrap()
    {
        $productData = [
            'specifications' => [],
            'variations' => [],
            'images' => [],
            'stock' => 1,
        ];

        $crawler = HtmlDomParser::str_get_html($this->html);
        // Get and set product name
        $productData['title'] = cleanString($crawler->findOne('h1[data-locator="pdp-productnametext"]')->text());

        //Get and set description
        $productData['description'] = substr(cleanString(strip_tags($crawler->findOne('#showMore')->outerHtml())), 8);

        //Get and set brand
        $brand = $crawler->findOne('a[itemprop="brand"]')->innerHtml;
        if ($brand) {
            $productData['brand'] = $crawler->findOne('a[itemprop="brand"] span')->innerText;
        }

        //Get and set SKU
        $url = $crawler->findOne('meta[property="og:url"]')->getAttribute('content');
        $urlToArray = explode('/', $url);
        $productData['SKU'] = $urlToArray[count($urlToArray) - 1];

        //Send request by product SKU for get detailed info about product
        $productSpecificationsRequest = file_get_contents('https://www.bedbathandbeyond.com/apis/stateless/v1.0/sku/product?product=' . $productData['SKU']);

        //Decoding product data json
        $specifications = json_decode($productSpecificationsRequest, 1)['data'];
        foreach ($specifications as $specification) {

            $images = explode(',', $specification['ALT_IMG']);
            //Set images
            foreach ($images as $key => $image) {
                $imageUrl = 'https://b3h2.scene7.com/is/image/BedBathandBeyond/' . $image . '?$690$&wid=690&hei=690';
                if (!in_array($imageUrl, $productData['images'])) {
                    $productData['images'][] = $imageUrl;
                }
            }

            //Set specifications
            $value = $specification['COLOR'];
            if (isset($specification['SKU_SIZE']) && $specification['SKU_SIZE'] != 'NO SIZE') {
                $value = htmlspecialchars_decode($specification['SKU_SIZE']);
            }

            //Get and set variations with variation type,price and stock availability
            $productData['variations'][] = ['value' => $value, 'price' => $specification['IS_PRICE']];
            $array = ['price' => cleanString($specification['IS_PRICE']), 'text' => ''];
            if (isset($specification['WAS_PRICE']) && !empty($specification['WAS_PRICE'])) {
                $array['was_price'] = cleanString($specification['WAS_PRICE']);
            }
            if (isset($productData['ONLINE_INVENTORY']) && $productData['ONLINE_INVENTORY'] == 'false') {
                $array['stock'] = 0;
                $array['outStockReason'] = "";
            }

            //Get and Set product price
            if (!isset($productData['price'])) {
                $productData['price'] = cleanString($specification['IS_PRICE']);
            }

            //Get and set product was price
            if (!isset($productData['was_price']) && (isset($array['was_price']) && !empty($array['was_price']))) {
                $productData['was_price'] = cleanString($specification['WAS_PRICE']);
            }

            //Get and set product specifications
            if (isset($specification['SPECS'])) {
                foreach ($specification['SPECS'] as $specItem) {
                    foreach ($specItem as $key => $value) {
                        if ($key == "Part or Model Number") {
                            $array['model'] = $value;
                        }
                        $array['text'] .= $key . ' ' . $value . "\n";
                    }
                }
                $productData['specifications'][] = $array;
            }
        }
        return $productData;
    }
}
