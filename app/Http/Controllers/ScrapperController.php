<?php

namespace App\Http\Controllers;

use Goutte\Client;

class ScrapperController extends Controller
{
    /**
     * Scrap Data
     */
    public function amazon()
    {
        // Create Scrapper Client Instance
        $client = new Client();
        // Product URL
        $url = 'https://www.amazon.co.uk/Winning-Moves-29612-Trivial-Pursuit/dp/B075716WLM/';
        $crawler = $client->request('GET', $url);
        $productData = [];

        // Get product title
        $crawler->filter('#productTitle')->each(function ($title) use (&$productData) {
            // Set product title
            $productData['title'] = cleanString($title->text());
        });

        // Get product price
        $crawler->filter('.sims-fbt-total-price .p13n-sc-price')->each(function ($price) use (&$productData) {
            // Set product price
            $productData['price'] = cleanString($price->text());
        });

        // Get product full description link
        $crawler->filter('#productDescription .a-link-normal')->each(function ($description) use ($client, &$productData) {
            // Go to that link
            $description = $client->click($description->link());

            // Get product description
            $description->filter('#productDescription')->each(function ($descriptionContent) use (&$productData) {
                // Set product description
                $productData['description'] = cleanString($descriptionContent->text());
            });
        });

        // Get product asin column line
        $crawler->filter('#prodDetails .col2 .techD .pdTab tr:nth-child(1)')->each(function ($asinBlock) use (&$productData) {
            // Get product asin value
            $asinBlock->filter('.value')->each(function ($asinValue) use ($productData) {
                // Set ASIN
                $productData['asin'] = cleanString($asinValue->text());
            });
        });

        // Get product specification line
        $crawler->filter('#prodDetails .col1 .techD .pdTab tr')->each(function ($line) use (&$productData) {
            // Get product specification title
            $line->filter('.label')->each(function ($label) use (&$productData, $line) {
                // Set product specification title and value
                $value = $line->filter('.value')->each(function ($value) use (&$productData) {
                    return $value->text();
                });
                // Create label property name from title
                $labelProperty = snake_case($label->text());
                // Set product specification data
                $productData['product_specifications'][$labelProperty] = $value[0];
            });

        });

        // Get product images
        $crawler->filter('.a-button-thumbnail img')->each(function ($img) use (&$productData) {
            // Get image src
            $imgSrc = $img->attr('src');

            // Check that we give only product images
            if (!strpos($imgSrc, 'x-locale/common/')) {
                // Replace img url for can give big img and save product image
                $productData['images'][] = str_replace('SS40', 'SS460', $imgSrc);
            }
        });
        return response()->json($productData);
    }

    public function bedBath()
    {
        $scrapper = new BedBathController(file_get_contents(asset('html4.html')));
        return response()->json($scrapper->scrap());
    }
}
